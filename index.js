const express = require('express')
const app = express()
const router = require('./router/routerFiles')
const PORT = process.env.PORT || 8080
app.use(express.json())
app.use('/api/files', router )
app.use((err, req, res, text) =>{
    console.log(err.status)
    res.status(500)
    res.json({message:'Server error'})
})


app.listen(PORT, () => {
    console.log(`Server has been launched at port ${PORT}`)
})