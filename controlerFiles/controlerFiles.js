const fs = require('fs')
const path = require('path')

const ext = ['.xml', 'yaml', '.txt', '.json', '.js', '.log']

const createFile = (req, res) => {
    const {filename, content} = req.body
    fs.mkdir(path.join(__dirname, '..' ,'files'), { recursive: true }, (err) => {
        if (err) {
            res.status(500)
            res.json({message:'Server error'}) 
            return
        }
    })
    
    if (fs.existsSync(path.join(__dirname, '..' ,'files', filename))) {
        res.status(400)
        res.json({message: "File exists in this directory"})
        return
    }

    if (!filename.trim()) {
        res.status(400)
        res.json({message: "Please specify 'filename' parameter"})
    }

    if (filename.trim() && content.trim()) {
        if (ext.includes(path.extname(filename))) {
                fs.writeFile(path.join(__dirname, '..' , 'files', filename), content, (err) => {
                    if(err) {
                        res.status(400)
                        res.json({message:'Server error'}) 
                        return   
                    }
                    res.status(200)
                    res.send({message: "File created successfully"})
                })
        } else {
            res.status(400)
            res.json({message: "Application can not support file with this extension"})
        }
    } else if (!filename || !content.trim()) {
        res.status(400)
        res.json({message: "Please specify 'content' parameter"})     
    }
}

const getFiles = (req, res) => {
    fs.readdir(path.join(__dirname, '..' ,'files'), (err, files) => {
        const sortedFiles = []
        if (err) {
            res.status(500)
            res.json({message:'Server error'}) 
            return
        } 
        files.forEach(file => {
            if (ext.includes(path.extname(file))) {
                sortedFiles.push(file)
            }
        }) 
        if (!sortedFiles.length) {
            res.status(400)
            res.json({
                message: "Client error",
            })
            return
        }
        res.status(200)
        res.json({
            message: "Success",
            files: sortedFiles
        })
    })
}

const getFile = (req, res) => {
    const {filename} = req.params
    fs.readFile(path.join(__dirname, '..' ,'files', filename), (err, data) => {
        if (err) {
            res.status(400)
            res.json({message: `No file with ${filename} filename found`})
        }else {
            let fileTime
            try {
                fileTime = fs.statSync(path.join(__dirname, '..' , 'files' , filename)).mtime
            } catch (error) {
                res.status(500)
                res.json({message:'Server error'}) 
                return
            }
            res.status(200)
            res.json({
                message: 'Success',
                filename: filename,      
                content: data.toString(),
                extension: path.extname(filename).split('.')[1],
                uploadedDate: fileTime
            })
        }
    })  
}

const modifyFile = (req, res) => {
    const paramsFile = req.params.filename
    const {filename, content} = req.body
    fs.readdir(path.join(__dirname, '..' ,'files'), (err, files) => {
       const file = files.find(item => {
           if (item === filename && item === paramsFile) {
               return item
           } 
        })
       if (err) {
            res.status(500)
            res.json({message:'Server error'}) 
            return
       }else if (!content.trim()) {
            res.status(400)
            res.json({message: "Please specify 'content' parameter"})
            return
       }
       if (file) {
        fs.writeFile(path.join(__dirname, '..' ,'files', filename), content , (err) => {
            if(err) {
                res.status(500)
                res.json({message:'Server error'}) 
                return
            }
            res.status(200)
            res.send({message: "File has been just updated"})
        })
       } else {
            res.status(400)
            res.json({message: `No file found`})
       }
    })
}

const deleteFile = (req, res) => {
    const paramsFile = req.params.filename
    const {filename} = req.body
    fs.readdir(path.join(__dirname, '..' ,'files'), (err, files) => {
       const file = files.find(item => {
           if (item === filename && item === paramsFile) {
               return item
           } 
        })
       if (err) {
            res.status(500)
            res.json({message:'Server error'}) 
            return
       }
       if (file) {
            fs.unlink(path.join(__dirname, '..' ,'files', filename), (err) => {
                if (err) {
                    res.status(500)
                    res.json({message:'Server error'}) 
                    return
                } 
                    res.status(200)
                    res.send({message: "File has been just deleted"})
            })
       } else {
            res.status(400)
            res.json({message: `No file found`})
        }
    })
}


module.exports = {createFile, getFiles, getFile, modifyFile, deleteFile}