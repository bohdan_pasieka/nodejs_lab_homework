const {Router} = require('express')
const {getFiles, createFile, getFile, modifyFile, deleteFile} = require('../controlerFiles/controlerFiles')
const router = Router()

router.get('/', getFiles)
router.post('/', createFile)
router.get('/:filename', getFile) 
router.delete('/:filename', deleteFile)
router.put('/:filename', modifyFile)

module.exports = router